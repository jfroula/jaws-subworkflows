workflow blastn_sub_wf {
    meta {
        author: 'Jeff Froula'
    }

    ### from inputs.json:
    File queryFile
    Int shards
    String db_prefix
    String? blast_options
	String? outputName
    
    call verify_table_format {
        input: options=blast_options
    }

    call shard {
        input: query = queryFile,
        shards=shards
    }

    Array[String] shard_array = shard.shard_index

    # For each ref file defined in the inputs json, blast query with ref in parallel.
    #
    scatter(coords in shard_array) {
        call runBlast{
            input: query=queryFile,
                   coords=coords,
                   blast_options=blast_options,
                   shards=shards,
                   db_prefix=db_prefix
       }
    }

    call mergeBlast{
        input: blastout_array=runBlast.blastout,
               outputName=outputName
    }

    output {
       File blastout = mergeBlast.merged
    }
}

### ------------------------------------ ###
task verify_table_format {
    String? options

    command {
        python<<CODE
        import re,sys
        if not re.search(r"outfmt\s+.\s*6", "${options}"):
            sys.exit("This doesn't look like table output format. " +
                     "Please use -outfmt and choose 6.") 
        CODE
    }

}

task shard {
    File query
    String bname = basename(query)
    Int shards

    command <<<
        set -e -o pipefail

        TOTAL=$(wc -c ${query} | awk '{print $1}')
        chunk_size=$((TOTAL/${shards}))
        echo $chunk_size
    
        fasta_indexer.py --input ${query} --output ${bname}.index

        create_blocks.py -if ${bname}.index -ff ${query} -of ${bname}.sharded -bs $chunk_size
	>>>

    runtime {
		docker: "jfroula/jaws-sub-blastn:1.0.2"
        cluster: "cori"
        time: "00:30:00"
        cpu: 1 
        mem: "5G"
        poolname: "small"
		nodes: 1
        nwpn: 1
    }

    output {
        Array[String] shard_index = read_lines("${bname}.sharded")
    }
}

task runBlast {
    File query
    String coords
    String? blast_options
    Int shards
    String db_prefix

    command <<< 
        #DEFAULT_OUTFMT = " -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles' "
        #DEFAULT_BLAST_OPTIONS = " -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -num_alignments 5 %s " % (DEFAULT_OUTFMT)
        #FUNGAL_BLAST_OPTIONS = " -task megablast -perc_identity 90 -evalue 1e-30 -dust \"yes\" -num_threads 8 %s " % (DEFAULT_OUTFMT)

        if [[ "${blast_options}" ]]; then
            FINAL_OPTIONS=${blast_options}
        else
            FINAL_OPTIONS=$DEFAULT_BLAST_OPTIONS 
        fi

        start=$(echo ${coords} | awk '{print $1}')
        end=$(echo ${coords} | awk '{print $2}')

        # TODO jfroula: we need to add a preceding space to blast_options because 
        # a bug in argparse won't recognize arguments begining with "-" (i.e. -p "-num_threads 8")
		# right now a space has to be provided in the inputs.json

        # we are piping a block of the fastq sequence to the aligner 
        shard_reader.py -i ${query} -s $start -e $end | \
        blastn -db /refdata/${db_prefix}/${db_prefix} \
          -query -  \
          -out blastout $FINAL_OPTIONS
    >>>

    runtime {
		docker: "jfroula/jaws-sub-blastn:1.0.2"
        cluster: "cori"
        time: "00:30:00"
        cpu: 1 
        mem: "5G"
        poolname: "small"
		nodes: 1
		nwpn: 1
    }

    output {
        File blastout = "blastout"
    }
}

task mergeBlast{
    Array[File] blastout_array
    String? outputName

    command {
        # merge and sort by evalue
        cat ${sep=' ' blastout_array} | sort -k4,4 > ${default="blastout" outputName}.merged.fa
    }

    runtime {
		docker: "jfroula/jaws-sub-blastn:1.0.2"
        cluster: "cori"
        time: "00:30:00"
        cpu: 1 
        mem: "5G"
        poolname: "small"
		nodes: 1
        nwpn: 1
    }

    output{
        File merged = glob("*.merged.fa")[0]
    }
}
