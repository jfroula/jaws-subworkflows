import "blastn_subworkflow.wdl" as blastn_sub

workflow main_wf { 
	File queryFile
	String db_prefix

	# this task calls the sub-workflow named blastn_sub 
	# It's output is "blastout"
    call blastn_sub.blastn_sub_wf { 
           input: queryFile = queryFile,
                  db_prefix=db_prefix
    }
    call do_something_with_output {
           input: myinput = blastn_sub_wf.blastout
    }
}

task do_something_with_output {
    File myinput

    command {
	  echo We have access to ${myinput}
    }

	output {
		File myout = stdout()
	}
}

