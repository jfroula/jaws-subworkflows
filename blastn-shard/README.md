# Summary
This is a repository for all the subworkflows available to JAWS users.  

# Subworkflows and how to use them
## blastn subworkflow
This implementation of blast uses sharding. Sharding is when the query fasta is split up into blocks and passed to blast via pipes and not files.  This is fast and performs blast in parallel.

To test run the subworkflow, you can run main.wdl that will call it. This way, you have an example how to impliment it as a task in your workflow.
```
# open up the shifter.conf file and set
dockerRoot = <current_working_dir>/cromwell-executions
java -Dconfig.file=shifter.conf -jar /global/dna/projectdirs/DSI/workflows/cromwell/java/cromwell.jar run main.wdl -i inputs.main.json
```

**Requirements and Assumptions:**
* The output format is tabular (i.e. -outfmt 6). This is so all the output blast files produced by running blast in parallel, are merged properly.
* docker image used: **jfroula/jaws-sub-blastn:1.0.2** see https://hub.docker.com/repository/docker/jfroula/jaws-sub-blastn
