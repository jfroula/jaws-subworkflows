#!/usr/bin/env python
#
# Create a list of commands for each shard
#
# ex) 1k bp block size for last run
# $ create_tasks.py -ff arctic_10.faa -if arctic_10.faa.idx -of arctic_10.faa.task -b 1000000 -dn arctic_10.faa.db
#
# Seung-Jin Sul (ssul@lbl.gov)
#
import argparse

STARTENDFORMAT = "%(startOffset)s\t%(endOffset)s\n"

if __name__ == '__main__':
    ## usage = "python create_tasks.py -i indexFile -o outputTaskFileName -s blockSize"
    desc = "shard creator"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-if", "--index_file",
                        dest="indexFile",
                        help="Fasta index file",
                        required=True)
    parser.add_argument("-ff", "--fasta_file",
                        dest="fastaFile",
                        help="Fasta file",
                        required=True)
    parser.add_argument("-of", "--output_file",
                        dest="outTaskFileName",
                        help="Output task file name",
                        required=True)
    parser.add_argument("-bs", "--block_size",
                        dest="blockSize",
                        help="Query block size in byte",
                        required=True,
                        type=int)
    options = parser.parse_args()

    with open(options.outTaskFileName, 'w') as TaskFH:
        with open(options.indexFile, 'r') as indexFH:
            ## index file line format: start_floc, seq_len, seq_num
            accSeqLen = 0
            blockStart = 0
            blockEnd = 0
            l = indexFH.readline()
            lastBlockEndRead = 0
            blockNum = 0

            while l:
                startLoc, endLoc, seqLen, seqNum = l.split()
                lastBlockEndRead = endLoc

                if accSeqLen == 0: blockStart = startLoc
                accSeqLen += int(seqLen)

                if accSeqLen >= options.blockSize:
                    blockEnd = endLoc
                    TaskFH.write(STARTENDFORMAT % {"startOffset": blockStart,
                                                   "endOffset": blockEnd})
                    accSeqLen = 0
                    blockNum += 1

                l = indexFH.readline()

            if int(blockEnd) < int(lastBlockEndRead):
                TaskFH.write(
                    STARTENDFORMAT % {"startOffset": blockStart,
                                      "endOffset": lastBlockEndRead})
                blockNum += 1

