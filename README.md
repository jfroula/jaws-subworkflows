# Summary
This is a repository for all the subworkflows available to JAWS users.
See the README files in each directory for more information on usage.

# Subworkflow Catalog
* blastn subworkflow 

	This worklfow shards input files and does blastn. Reference databases are user defined and blast options can be specified, except for -outfmt which is restricted to tabular format (-outfmt 6).

